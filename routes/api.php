<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function(){
    Route::post('/register', [App\Http\Controllers\AuthController::class, 'register']);
    Route::post('/login',  [App\Http\Controllers\AuthController::class, 'login']);
    Route::post('/logout',  [App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('/refresh',  [App\Http\Controllers\AuthController::class, 'refresh']);
    Route::get('/user',  [App\Http\Controllers\AuthController::class, 'user']);
});

Route::group(['middleware' => 'auth:api'], function(){

    Route::group(['prefix' => 'it'], function(){
        Route::group(['prefix' => 'ruangan'], function(){
            Route::get('/get-ruangan-all', [App\Http\Controllers\ITController::class, 'getRuanganAll']);
            Route::get('/get-ruangan', [App\Http\Controllers\ITController::class, 'getRuangan']);
            Route::post('/save-ruangan', [App\Http\Controllers\ITController::class, 'saveRuangan']);
            Route::put('/update-ruangan', [App\Http\Controllers\ITController::class, 'updateRuangan']);
            Route::delete('/delete-ruangan/{id_ruangan}', [App\Http\Controllers\ITController::class, 'delRuangan']);
        });
        Route::group(['prefix' => 'kamar'], function(){
            Route::get('/get-kamar-all', [App\Http\Controllers\ITController::class, 'getKamarAll']);
            Route::get('/get-kamar/{id_kamar}', [App\Http\Controllers\ITController::class, 'getKamar']);
            Route::post('/save-kamar', [App\Http\Controllers\ITController::class, 'saveKamar']);
            Route::put('/update-kamar', [App\Http\Controllers\ITController::class, 'updateKamar']);
            Route::delete('/delete-kamar/{id_kamar}', [App\Http\Controllers\ITController::class, 'delKamar']);
        });
        Route::group(['prefix' => 'kelas'], function(){
            Route::get('/get-kelas-all', [App\Http\Controllers\ITController::class, 'getKelasAll']);
            Route::get('/get-kelas/{id_kelas}', [App\Http\Controllers\ITController::class, 'getKelas']);
            Route::post('/save-kelas', [App\Http\Controllers\ITController::class, 'saveKelas']);
            Route::put('/update-kelas', [App\Http\Controllers\ITController::class, 'updateKelas']);
            Route::delete('/delete-kelas/{id_kelas}', [App\Http\Controllers\ITController::class, 'delKelas']);
        });
        Route::group(['prefix' => 'bed'], function(){
            Route::get('/get-bed-all', [App\Http\Controllers\ITController::class, 'getBedAll']);
            Route::get('/get-bed/{id_bed}', [App\Http\Controllers\ITController::class, 'getBed']);
            Route::post('/save-bed', [App\Http\Controllers\ITController::class, 'saveBed']);
            Route::put('/update-bed', [App\Http\Controllers\ITController::class, 'updateBed']);
            Route::delete('/delete-bed/{id_bed}', [App\Http\Controllers\ITController::class, 'delBed']);
        });
        Route::group(['prefix' => 'obat'], function(){
            Route::get('/get-obat-all', [App\Http\Controllers\ITController::class, 'getObatAll']);
            Route::get('/get-obat/{id_obat}', [App\Http\Controllers\ITController::class, 'getObat']);
            Route::post('/save-obat', [App\Http\Controllers\ITController::class, 'saveObat']);
            Route::put('/update-obat', [App\Http\Controllers\ITController::class, 'updateObat']);
            Route::delete('/delete-obat/{id_obat}', [App\Http\Controllers\ITController::class, 'delObat']);
        });
        Route::group(['prefix' => 'jenis-obat'], function(){
            Route::get('/get-jenis-obat-all', [App\Http\Controllers\ITController::class, 'getJenisObatAll']);
            Route::get('/get-jenis-obat/{id_jenis_obat}', [App\Http\Controllers\ITController::class, 'getJenisObat']);
            Route::post('/save-jenis-obat', [App\Http\Controllers\ITController::class, 'saveJenisObat']);
            Route::put('/update-jenis-obat', [App\Http\Controllers\ITController::class, 'updateJenisObat']);
            Route::delete('/delete-jenis-obat/{id_jenis_obat}', [App\Http\Controllers\ITController::class, 'delJenisObat']);
        });
        Route::group(['prefix' => 'harga-obat'], function(){
            Route::get('/get-harga-obat-all', [App\Http\Controllers\ITController::class, 'getHargaObatAll']);
            Route::get('/get-harga-obat/{id_harga_obat}', [App\Http\Controllers\ITController::class, 'getHargaObat']);
            Route::post('/save-harga-obat', [App\Http\Controllers\ITController::class, 'saveHargaObat']);
            Route::put('/update-harga-obat', [App\Http\Controllers\ITController::class, 'updateHargaObat']);
            Route::delete('/delete-harga-obat/{id_harga_obat}', [App\Http\Controllers\ITController::class, 'delHargaObat']);
        });
        Route::group(['prefix' => 'komponen-harga-obat'], function(){
            Route::get('/get-komponen-harga-obat-all', [App\Http\Controllers\ITController::class, 'getKomponenHargaObatAll']);
            Route::get('/get-komponen-harga-obat/{id_komponen_harga_obat}', [App\Http\Controllers\ITController::class, 'getKomponenHargaObat']);
            Route::post('/save-komponen-harga-obat', [App\Http\Controllers\ITController::class, 'saveKomponenHargaObat']);
            Route::put('/update-komponen-harga-obat', [App\Http\Controllers\ITController::class, 'updateKomponenHargaObat']);
            Route::delete('/delete-komponen-harga-obat/{id_komponen_harga_obat}', [App\Http\Controllers\ITController::class, 'delKomponenHargaObat']);
        });
        Route::group(['prefix' => 'tindakan'], function(){
            Route::get('/get-tindakan-all', [App\Http\Controllers\ITController::class, 'getTindakanAll']);
            Route::get('/get-tindakan/{id_tindakan}', [App\Http\Controllers\ITController::class, 'getTindakan']);
            Route::post('/save-tindakan', [App\Http\Controllers\ITController::class, 'saveTindakan']);
            Route::put('/update-tindakan', [App\Http\Controllers\ITController::class, 'updateTindakan']);
            Route::delete('/delete-tindakan/{id_tindakan}', [App\Http\Controllers\ITController::class, 'delTindakan']);
        });
        Route::group(['prefix' => 'jenis-tindakan'], function(){
            Route::get('/get-jenis-tindakan-all', [App\Http\Controllers\ITController::class, 'getJenisTindakanAll']);
            Route::get('/get-jenis-tindakan/{id_jenis_tindakan}', [App\Http\Controllers\ITController::class, 'getJenisTindakan']);
            Route::post('/save-jenis-tindakan', [App\Http\Controllers\ITController::class, 'saveJenisTindakan']);
            Route::put('/update-jenis-tindakan', [App\Http\Controllers\ITController::class, 'updateJenisTindakan']);
            Route::delete('/delete-jenis-tindakan/{id_jenis_tindakan}', [App\Http\Controllers\ITController::class, 'delJenisTindakan']);
        });
        Route::group(['prefix' => 'harga-tindakan'], function(){
            Route::get('/get-harga-tindakan-all', [App\Http\Controllers\ITController::class, 'getHargaTindakanAll']);
            Route::get('/get-harga-tindakan/{id_harga_tindakan}', [App\Http\Controllers\ITController::class, 'getHargaTindakan']);
            Route::post('/save-harga-tindakan', [App\Http\Controllers\ITController::class, 'saveHargaTindakan']);
            Route::put('/update-harga-tindakan', [App\Http\Controllers\ITController::class, 'updateHargaTindakan']);
            Route::delete('/delete-harga-tindakan/{id_harga_tindakan}', [App\Http\Controllers\ITController::class, 'delHargaTindakan']);
        });
        Route::group(['prefix' => 'komponen-harga-tindakan'], function(){
            Route::get('/get-komponen-harga-tindakan-all', [App\Http\Controllers\ITController::class, 'getKomponenHargaTindakanAll']);
            Route::get('/get-komponen-harga-tindakan/{id_komponen_harga_tindakan}', [App\Http\Controllers\ITController::class, 'getKomponenHargaTindakan']);
            Route::post('/save-komponen-harga-tindakan', [App\Http\Controllers\ITController::class, 'saveKomponenHargaTindakan']);
            Route::put('/update-komponen-harga-tindakan', [App\Http\Controllers\ITController::class, 'updateKomponenHargaTindakan']);
            Route::delete('/delete-komponen-harga-tindakan/{id_komponen_harga_tindakan}', [App\Http\Controllers\ITController::class, 'delKomponenHargaTindakan']);
        });
        Route::group(['prefix' => 'pegawai'], function(){
            Route::get('/get-pegawai-all', [App\Http\Controllers\ITController::class, 'getPegawaiAll']);
            Route::get('/get-pegawai/{id_pegawai}', [App\Http\Controllers\ITController::class, 'getPegawai']);
            Route::post('/save-pegawai', [App\Http\Controllers\ITController::class, 'savePegawai']);
            Route::put('/update-pegawai', [App\Http\Controllers\ITController::class, 'updatePegawai']);
            Route::delete('/delete-pegawai/{id_pegawai}', [App\Http\Controllers\ITController::class, 'delPegawai']);
        });
        Route::group(['prefix' => 'jenis-pegawai'], function(){
            Route::get('/get-jenis-pegawai-all', [App\Http\Controllers\ITController::class, 'getJenisPegawaiAll']);
            Route::get('/get-jenis-pegawai/{id_jenis_pegawai}', [App\Http\Controllers\ITController::class, 'getJenisPegawai']);
            Route::post('/save-jenis-pegawai', [App\Http\Controllers\ITController::class, 'saveJenisPegawai']);
            Route::put('/update-jenis-pegawai', [App\Http\Controllers\ITController::class, 'updateJenisPegawai']);
            Route::delete('/delete-jenis-pegawai/{id_jenis_pegawai}', [App\Http\Controllers\ITController::class, 'delJenisPegawai']);
        });
        Route::group(['prefix' => 'role'], function(){
            // Route::get('/get-role-all', [App\Http\Controllers\ITController::class, 'getRuanganAll']);
            // Route::get('/get-ruangan', [App\Http\Controllers\ITController::class, 'getRuangan']);
            Route::post('/save-role', [App\Http\Controllers\ITController::class, 'saveRole']);
            // Route::put('/update-ruangan', [App\Http\Controllers\ITController::class, 'updateRuangan']);
            // Route::delete('/delete-ruangan/{id_ruangan}', [App\Http\Controllers\ITController::class, 'delRuangan']);
        });
        Route::group(['prefix' => 'menu'], function(){
            // Route::get('/get-role-all', [App\Http\Controllers\ITController::class, 'getRuanganAll']);
            // Route::get('/get-ruangan', [App\Http\Controllers\ITController::class, 'getRuangan']);
            Route::get('/get-menu-by-role', [App\Http\Controllers\ITController::class, 'getMenuByRole']);
            Route::post('/save-menu', [App\Http\Controllers\ITController::class, 'saveMenu']);
            // Route::put('/update-ruangan', [App\Http\Controllers\ITController::class, 'updateRuangan']);
            // Route::delete('/delete-ruangan/{id_ruangan}', [App\Http\Controllers\ITController::class, 'delRuangan']);
        });
    });

    Route::group(['prefix' => 'rm'], function(){
        Route::group(['prefix' => 'pasien'], function(){
            Route::get('/get-pasien-all', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
            Route::get('/get-pasien/{id_pasien}', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
            Route::post('/save-pasien', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
            Route::put('/update-pasien', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
            Route::delete('/delete-pasien/{id_pasien}', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
        });
    });
    
    Route::group(['prefix' => 'perawat'], function(){
        Route::get('/get-pasien', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
    });
    
    Route::group(['prefix' => 'dokter'], function(){
        Route::group(['prefix' => 'resep'], function(){
            Route::get('/get-resep-all', [App\Http\Controllers\DokterController::class, 'getResepAll']);
            Route::get('/get-resep/{id_resep}', [App\Http\Controllers\DokterController::class, 'getResep']);
            Route::post('/save-resep', [App\Http\Controllers\DokterController::class, 'saveResep']);
            Route::put('/update-resep', [App\Http\Controllers\DokterController::class, 'updateResep']);
            Route::delete('/delete-resep/{id_resep}', [App\Http\Controllers\DokterController::class, 'delResep']);
        });
    });
    
    Route::group(['prefix' => 'farmasi'], function(){
        Route::group(['prefix' => 'resep'], function(){
            Route::get('/get-resep-all', [App\Http\Controllers\FarmasiController::class, 'getResepAll']);
            Route::get('/get-resep/{id_resep}', [App\Http\Controllers\FarmasiController::class, 'getResep']);
            Route::post('/save-resep', [App\Http\Controllers\FarmasiController::class, 'saveResep']);
            Route::put('/update-resep', [App\Http\Controllers\FarmasiController::class, 'updateResep']);
            Route::delete('/delete-resep/{id_resep}', [App\Http\Controllers\FarmasiController::class, 'delResep']);
        });
    });
    
    Route::group(['prefix' => 'kasir'], function(){
        Route::get('/get-pasien', [App\Http\Controllers\RekamMedisController::class, 'getPasien']);
    });    
});


