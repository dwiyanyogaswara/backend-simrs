<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ITModel;

class ITController extends Controller
{
    public function getRuanganAll(){
        $result = ITModel::readRuanganAll();
        return $result;
    }

    public function getRuangan(){
        $data = $_GET;
        $result = ITModel::readRuangan($data);
        
        return $result;
    }

    public function saveRuangan(Request $request){
        $data = $request->post();
        $insert_ruangan = ITModel::createRuangan($data);
        
        if($insert_ruangan == 1){
            $result= [
                "message"=>"success"
            ];
        } else {
            $result= [
                "message"=>"error"
            ];
        }
        return $result;
    }

    public function getMenuByRole(Request $request){
        $data = $request->query();
        $menu = ITModel::readMenuByRole($data);

        $result = [];
        foreach($menu as $key => $val){
            if(empty($menu[$key]['parent'])){
                array_push($result, $menu[$key]);
                unset($menu[$key]);
            }
            
        }
        sort($menu);
        
        foreach($result as $key => $val){
            $result[$key]['submenu']=[];
            foreach($menu as $k => $v){
                if($menu[$k]['parent'] == $result[$key]['id']){
                    array_push($result[$key]['submenu'], $menu[$k]);
                }
            }
        }

        return $result;
    }

    // public function getRuangan(){
    //     $data = $_GET;
    //     $result = ITModel::getRuangan($data);
        
    //     return $result;
    // }

    public function saveMenu(Request $request){
        $data = $request->post();
        $insert_menu = ITModel::createMenu($data);
        
        if($insert_menu == 1){
            $result= [
                "message"=>"success"
            ];
        } else {
            $result= [
                "message"=>"error"
            ];
        }
        return $result;
    }

    public function saveRole(Request $request){
        $data = $request->post();
        $insert_role = ITModel::createRole($data);
        
        if($insert_role == 1){
            $result= [
                "message"=>"success"
            ];
        } else {
            $result= [
                "message"=>"error"
            ];
        }
        return $result;
    }
}
