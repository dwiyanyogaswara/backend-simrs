<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RekamMedisController extends Controller
{
    public function getPasien(){
        header('Access-Control-Allow-Origin: *');
        $pasien= array(
            [
                "id"=>"123",
                "namapasien"=>"Dalijo"
            ],
            [
                "id"=>"124",
                "namapasien"=>"Parni"
            ],
            [
                "id"=>"125",
                "namapasien"=>"Bambang"
            ]
        );
        return json_encode($pasien);
    }
}
