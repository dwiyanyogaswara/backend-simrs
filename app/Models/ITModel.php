<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ITModel extends Model
{
    public static function readRuanganAll(){
        $result = DB::table('ruangan_m')->get();
        return json_decode(json_encode($result), true);
    }

    public static function readRuangan($data){
        if(isset($data['id'])){
            $result = DB::table('ruangan_m')->where('id', $data['id'])->get();
        } else if(isset($data['nama'])){
            $result = DB::table('ruangan_m')->where('nama', $data['nama'])->get();
        }
        
        return json_decode(json_encode($result), true);
    }

    public static function createRuangan($data){
        $insert_ruangan = DB::table('ruangan_m')->insert(array('nama'=>$data['nama'],
                                                                'isactive'=>1));
        
        return $insert_ruangan;
    }

    public static function readMenuByRole($data){
        $result = DB::table('menu_m')->where('id_role', $data['id_role'])
                                     ->where('id', '!=', $data['id_role'])->get();

        return json_decode(json_encode($result), true);
    }

    public static function createMenu($data){
        $insert_menu = DB::table('menu_m')->insert(array('id_role'=>$data['id_role'],
                                                            'menu'=>$data['menu'],
                                                            'title'=>isset($data['title'])?$data['title']:NULL,
                                                            'path'=>isset($data['path'])?$data['path']:NULL,
                                                            'parent'=>isset($data['parent'])?$data['parent']:NULL,
                                                            'no'=>isset($data['no'])?$data['no']:1,
                                                            'isactive'=>1));
        
        return $insert_menu;
    }

    public static function createRole($data){
        $result = DB::table('role_m')->insert(array('role'=>$data['role'],
                                                         'isactive'=>1));
        
        return json_decode(json_encode($result), true);
    }
}
