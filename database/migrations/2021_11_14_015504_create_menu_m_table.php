<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_m', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_role');
            $table->string('menu');
            $table->string('title')->nullable();
            $table->string('path')->nullable();
            $table->integer('parent')->nullable();
            $table->integer('no')->default(1);
            $table->integer('isactive');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_m');
    }
}
